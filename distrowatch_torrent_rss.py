#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 18:15:00 2018

@author: antoine
"""

## sudo apt install python3-feedparser

import os
import feedparser
import urllib

URL =  "https://distrowatch.com/news/torrents.xml"

DIR_PATH = os.path.dirname(os.path.realpath(__file__))
TORRENT_LOCATION = DIR_PATH + "/DistrowatchTorrentRSS/"

class Torrent:
    "class to represent a torrent"
    def __init__(self, name, torrent_name, torrent_link):
        self.name =  name
        self.torrent_name = torrent_name
        self.torrent_link = torrent_link
    def download(self):
        "to download the torrent"
        if not os.path.exists(TORRENT_LOCATION):
            os.makedirs(TORRENT_LOCATION)

        filename = TORRENT_LOCATION + self.torrent_name
        if os.path.isfile(filename) or os.path.isfile(filename + ".added"):
            print("Already in the directory : ", self.name)
        else:
            print("Downloading : ", self.name)
            urllib.request.urlretrieve(self.torrent_link, filename)

    def __repr__(self):
        return self.name + " : " + self.torrent_name
    
class DistrowatchRSS:
    "class to download on distrowatch page"
    def __init__(self):
        self.url = URL

    def get_torrents(self):
        "to get all the torrent on the page"
        
        rss = feedparser.parse(self.url)
        
        nb_torrent = len(rss.entries)
        torrents = []
        for n in range(nb_torrent):
            torrent_name = rss.entries[n].title
            name = torrent_name.replace("https://distrowatch.com/dwres/torrents/","")
            torrent_link = rss.entries[n].link
            torrent=Torrent(name, torrent_name, torrent_link)
            #print(torrent)
            #print(torrent.torrent_link)
            torrents.append(torrent)

        return torrents

    def download(self, nb = None):
        """to download all the torrent on the page
           nb to get only the last nb torrent
        """
        
        if nb is None :
            torrents_to_download = self.get_torrents()
        else :
            torrents_to_download = self.get_torrents()[0:nb]
            
        for torrent in torrents_to_download:
            torrent.download()
            
    def torrent_list(self):
        "returns all the available torrent"
        for torrent in self.get_torrents():
            print(torrent)

    def __repr__(self):
        return str(len(self.get_torrents())) + " torrents"

# =============================================================================
if __name__ == "__main__":
    print("This program download every torrent on distrowatch.com rss feed")
    DP = DistrowatchRSS()
    #DP.download(10)
    DP.download()