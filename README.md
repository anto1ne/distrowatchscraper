# DWS (Distrowatch Scraper)

Web scraper to download every .torrent avalaible on [distrowatch.com](https://distrowatch.com/)

This software download every .torrent available on distrowatch.com

Torrents are store in "TORRENT_LOCATION", you can then open them
with transmission and share free software.

Enjoy ;)

## How to

Simply start this script with the following command

`python3 distrowatch_torrent.py`

or 

`python3 distrowatch_torrent_rss.py`


Think to configure transmission to make sure that it adds automatically all the torrent.

## Author

toitoinebzh

## License

GPLv3

## Icon

Icon comes from openclipart.org and can be found [here](https://openclipart.org/detail/212157/bittorrent-torrent-file).